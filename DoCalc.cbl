       IDENTIFICATION DIVISION. 
       PROGRAM-ID. DoCalc.
       AUTHOR. Saponchet Warakidsathapohn.
       DATA DIVISION. 
       WORKING-STORAGE SECTION. 
       01 FirstNum     PIC 9          VALUE ZEROS.
       01 SecondNum    PIC 9          VALUE ZEROS.
       01 CaleResult   PIC 99         VALUE 0.
       01 UserPrompt   PIC X(38)      VALUE 
                       "Please enter two single digit numbers".
       PROCEDURE DIVISION.
       CaculateResult.
           DISPLAY UserPrompt 
           ACCEPT FirstNum 
           ACCEPT SecondNum 
           COMPUTE CaleResult = FirstNum + SecondNum 
           DISPLAY "Result is = ", CaleResult 
           STOP RUN .